﻿using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;


namespace Simple_Music_Player
{
    public sealed partial class MainPage : Page
    {
        public PlayList List;
        public MainPage()
        {
            this.InitializeComponent();
            List = new PlayList();
            
        }
        private async void AddButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            await List.AddItem(playList, mediaPlayer);
        }
        private async void Move_To_Song(object sender, RoutedEventArgs e)
        {
            await List.MoveToSong(playList, mediaPlayer);
        }
        private async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            await List.RemoveItem(playList, mediaPlayer);
        }

        private void MediaTransportControls_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Space)

                if (mediaPlayer.MediaPlayer.PlaybackSession.PlaybackState == MediaPlaybackState.Playing)
                {
                    mediaPlayer.MediaPlayer.Pause();
                }
                else if (mediaPlayer.MediaPlayer.PlaybackSession.PlaybackState == MediaPlaybackState.Paused)
                {
                    mediaPlayer.MediaPlayer.Play();
                }
        }

    }
}
