﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Controls;

namespace Simple_Music_Player
{

    public class PlayList
    {
        public MediaPlaybackList _mediaPlaybackList;
        public PlayList()
        {
            _mediaPlaybackList = new MediaPlaybackList();
        }
        public async  System.Threading.Tasks.Task AddItem(ListView listView, MediaPlayerElement mediaPlayerElement)
        {

            var filePicker = new Windows.Storage.Pickers.FileOpenPicker();
            string[] fileTypes = new string[] { ".wmv", ".mp3", ".mp4", ".wma" };
            foreach (string fileType in fileTypes)
            {
                filePicker.FileTypeFilter.Add(fileType);
            }

            filePicker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            var pickedFiles = await filePicker.PickMultipleFilesAsync();

            foreach (var file in pickedFiles)
            {
                var mediaPlaybackItem = new MediaPlaybackItem(MediaSource.CreateFromStorageFile(file));
                _mediaPlaybackList.Items.Add(mediaPlaybackItem);
                listView.Items.Add(file.DisplayName);
            }
            _mediaPlaybackList.AutoRepeatEnabled = true;
            mediaPlayerElement.Source = _mediaPlaybackList;
        }
        public async System.Threading.Tasks.Task RemoveItem(ListView listView, MediaPlayerElement mediaPlayerElement)
        {
            for(int i = 0;i< listView.Items.Count(); i++)
            {
                if (listView.SelectedItem.Equals(listView.Items[i]))
                {
                    _mediaPlaybackList.Items.RemoveAt(i);
                    listView.Items.RemoveAt(i);
                }
            }
            mediaPlayerElement.Source = _mediaPlaybackList;
        }
        public async System.Threading.Tasks.Task MoveToSong(ListView listView, MediaPlayerElement mediaPlayerElement)
        {
            for (int i = 0; i < listView.Items.Count(); i++)
            {
                if (listView.SelectedItem.Equals(listView.Items[i]))
                {
                    Debug.WriteLine(i);
                    _mediaPlaybackList.MoveTo((UInt32)i);
                }
            }
            
        }
    }
}
